""" Transmit SpeedECU to SM
# Python 2.7
# Author : Fakhrul Zakuan
# Email : razi@moovita.com
# Department : eMooVit, MY
# Last modified: 17-August-2018
# version comments - Simplified CAN read
# Documentation says Little Endian but weird bit ordering
# LSB 0, MSB 15, but the bit order is actually MSB byte0[7, 6 , 5 , 4, 3 , 2 , 1 , 0] byte1[15, 14 , 13 , 12 ,11 , 10, 9 , 8] LSB
# From this looks its actually Big Endian Byte Order
"""

import can
import binascii
import time

bus = can.Bus(interface='socketcan',
              bustype='socketcan_ctypes',
              channel='can0',
              receive_own_messages=True)

byte = [0] * 8
kmh_byte_dec = [0] * 8

class VehicleDataSpeed:

    def __init__(self, msg):
        try:
            self.msg = msg
            byte8 = binascii.hexlify(msg.data)
            self.ID = hex(msg.arbitration_id).replace('L', '')[2:]
            self.bin64 = bin(int(byte8, 16))[2:].zfill(64)
        except:
            print "No CAN Data. Please Check Connection"

    def speedtoSM(self):

        if self.ID == "208":
            # print self.ID
            kmh_dec = int(self.bin64[0:16], 2)
            # Factor to KMH
            speed_kmh = kmh_dec * 0.02
            //print speed_kmh
            
            if speed_kmh > 327.67:
                speed_kmh = 327.67
            # Prepare bit ordering to sens to SM
            speed_kmh1 = speed_kmh * 100

            #data bit location
            kmh_bin = "{0:015b}".format(int(speed_kmh1))  # SM needs 15bits
            lateral_acc = '1' * 8
            nof_inc = '0' * 6
            speed_directness = '0'
            indicator_left = '0'
            indicator_right = '0'
            dimming = '0' * 7
            reserved = '0' * 25

            speed_bits = reserved + dimming + indicator_right + indicator_left + speed_directness + nof_inc + lateral_acc + kmh_bin
            byte[7] = speed_bits[0:8]
            byte[6] = speed_bits[8:16]
            byte[5] = speed_bits[16:24]
            byte[4] = speed_bits[24:32]
            byte[3] = speed_bits[32:40]
            byte[2] = speed_bits[40:48]
            byte[1] = speed_bits[48:56]
            byte[0] = speed_bits[56:64]

            for k, u in zip(byte, range(len(byte))):  # This for creates Delay
                kmh_byte_dec[u] = int("{0:0>2d}".format(int(k, 2)))  # > symbol little endian
            kmh_can = can.Message(arbitration_id=0x3f5, data=kmh_byte_dec, extended_id=False)  # Hex decimal value
            time.sleep(0.01)
            bus.send(kmh_can)
            print kmh_can
            
if __name__ == "__main__":
    for _msg in bus:
        _VehicleData = VehicleDataSpeed(_msg)
        VD =_VehicleData.speedtoSM()
